# Matoha unified message format

Across our communication interfaces, we use the same communication protocol, regardless if it is CAN, Bluetooth or some
other protocol. This format relies on [Protocol Buffers](https://en.wikipedia.org/wiki/Protocol_Buffers), the documentation
for which can be found on [Google's pages](https://developers.google.com/protocol-buffers/). Protocol Buffers (PB) allow
us to easily and efficiently encode the messages and transmit them in limited bandwidth. Google provides PB libraries for
all major languages; in Matoha we internally use them with our C++ (device firmware), Javascript/React (mobile app) and Python
(communication libraries) and we can provide you with implementation support for these languages.

## Protocol documentation

The source code has been thoroughly documented, please see the `matoha_message.proto` file.


## Usage

PB have to be compiled for each language. For your convenience, we have prepared a script for this, `compile_protobufs.sh`.
To run it, see the following sections

### Pre-requisites

- Protocol Buffer compiler, protoc (installation instructions are here: https://github.com/protocolbuffers/protobuf)
    - on Debian: `sudo apt-get install protobuf-compiler` 
- Python 3 with the libraries listed in requirements.txt installed
(run `pip3 install -r requirements.txt`)

**Note**: It appears that the latest versions of Protobuf have issues with generating the JS files. We recommend using
version 3.18 (https://github.com/protocolbuffers/protobuf/releases/tag/v3.18.0)

You can also see the CI file (`.gitlab-ci.yml`) to see how we install them on a headless Debian machine.

### Generating the protocol buffer files

To generate the compiled PB files, run the script `compile_protobufs.sh` as shown below. You will need Python3 installed.
This will generate the protobuf files for the following languages

- C/C++ (.pb.c and .pb.h)
- Javascript (.js)
- Python (.py)

```
./compile_protobufs.sh
```

### License
The source codes are (c) 2021 Matoha Instrumentation Ltd. and are licensed under the
open-source MIT license so that you can easily re-use the code. Please see the LICENSE file. 

This work relies on the NanoPB library by Petteri Aimonen https://github.com/nanopb/nanopb
A stripped-down version with minor modifications is attached in the `nanopb-generator` folder. The license for NanoPB can
be found in the same folder. The license terms can also be found in the same folder (ZLib license).