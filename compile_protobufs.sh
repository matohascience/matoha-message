#!/bin/bash
set -e

echo "Generating C/C++ protobuf files"
nanopb-generator/protoc --plugin=nanopb-generator/protoc-gen-nanopb --nanopb_out=. --proto_path="nanopb-generator/proto" -I=. matoha_message.proto
mv matoha_message.pb.h include/
echo "C/C++ protobuf generation done, matoha_message.pb.c and include/matoha_message.pb.h created"

echo "Generating Python protobuf files"
nanopb-generator/protoc --plugin=nanopb-generator/protoc-gen-nanopb --python_out=.  --proto_path="nanopb-generator/proto" -I=. matoha_message.proto
cp nanopb-generator/proto/nanopb_pb2.py .
echo "Python protobuf generation done, matoha_message_pb2.py created"

echo "Generating Javascript protobuf files"
nanopb-generator/protoc --plugin=nanopb-generator/protoc-gen-nanopb --js_out=import_style=commonjs,binary:./ --proto_path="nanopb-generator/proto" -I=. matoha_message.proto
# Remove the nanopb artifacts
sed -i -e "s/goog.object.extend(proto, nanopb_pb);//g" matoha_message_pb.js
sed -i -e "s/var nanopb_pb = require('.\/nanopb_pb.js');//g" matoha_message_pb.js
# On mac, use the below:
#sed "-i" "" "-e" "s/goog.object.extend(proto, nanopb_pb);//g" matoha_message_pb.js
#sed "-i" "" "-e" "s/var nanopb_pb = require('.\/nanopb_pb.js');//g" matoha_message_pb.js
echo "Javascript protobuf generation done, matoha_message_pb.js created"
